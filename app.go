package server

import (
	"flag"
	"fmt"
	"goCHAT_API/auth"
	"goCHAT_API/centrifugo"
	"goCHAT_API/database"
	"goCHAT_API/helpers"
	"goCHAT_API/logger"
	"goCHAT_API/logger/filelogger"
	"goCHAT_API/message"
	"goCHAT_API/server/env"
	"goCHAT_API/user/usermanager"
	"goCHAT_API/user/usermanager/dbusermanager"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strconv"

	message_handler_http "goCHAT_API/message/http"
	message_repository_mysql "goCHAT_API/message/repository/mysql"
	message_usecase "goCHAT_API/message/usecase"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/joho/godotenv"
)

type App struct {
	debug            bool
	pathToEnvFile    string
	messageUC        message.UseCase
	centrifugoClient *centrifugo.Centrifugo
	userManager      usermanager.UserManager
	logger           logger.InterfaceLogger
	Params           *env.Params
}

func NewApp() *App {
	//mr := mysql

	isDebug := flag.Bool("debug", false, "If this param is defined then application run in debug mode")
	pathToEnvFile := flag.String("config", ".", "Path to environment config file")
	flag.Parse()

	fmt.Printf("is debug: %t\n", *isDebug)
	fmt.Printf("path to env file: %s\n", *pathToEnvFile)

	// init logger
	executable, _ := os.Executable()
	executableDirectory, executableDirectoryErr := filepath.Abs(filepath.Dir(executable))
	if executableDirectoryErr != nil {
		log.Fatal("Undefined executebladirectory")
		os.Exit(0)
	}

	fmt.Println("executableDirectory:", executableDirectory)

	l := filelogger.NewLogger(executableDirectory)

	err := godotenv.Load(*pathToEnvFile)
	if err != nil {
		l.Error("Error loading .env file")
		os.Exit(0)
	}

	fmt.Println("Db connection")

	// local db
	host, dbName, userName, password := os.Getenv("HOST"), os.Getenv("DB_NAME"), os.Getenv("USER_NAME"), os.Getenv("PASSWORD")
	dsn := helpers.Dsn(host, userName, password, dbName)
	l.Infof("Database connect %s", dsn)
	db, initDbErr := database.NewDatabase(dsn)
	if initDbErr != nil {
		fmt.Println(initDbErr.Error())
		err := fmt.Errorf("Database connect error %s", initDbErr.Error())
		fmt.Println(err.Error())
		l.Errorf("Database connect error %s", initDbErr.Error())
		os.Exit(1)
	}
	db.Db.LogMode(*isDebug)

	// crm db
	crmhost, crmdbName, crmuserName, crmpassword := os.Getenv("CRM_DB_HOST"), os.Getenv("CRM_DB_DB_NAME"), os.Getenv("CRM_DB_USER_NAME"), os.Getenv("CRM_DB_PASSWORD")
	crmdsn := helpers.Dsn(crmhost, crmuserName, crmpassword, crmdbName)
	l.Info("Crm database connect " + crmdsn)
	crmdb, initCrmDbErr := database.NewDatabase(crmdsn)
	if initCrmDbErr != nil {
		l.Error(initCrmDbErr.Error())
		fmt.Println(initCrmDbErr.Error())
		os.Exit(1)
	}
	crmdb.Db.LogMode(*isDebug)

	//defer db.Db.Close()
	fmt.Println("Db connection success")

	messageRepository := message_repository_mysql.NewMessageRepository(db)
	params := env.CreateParams()

	centrifugoClient := centrifugo.NewCentClient(params, l)

	userManager := dbusermanager.NewUserManager(crmdb, db, os.Getenv("CHAT_ADMIN_ROLE_NAME"))
	
	envErr := godotenv.Load(*pathToEnvFile)
	if envErr != nil {
		//app.logger.Error("Error loading .env file")
		log.Fatal("Error loading .env file")
		os.Exit(0)
	}

	return &App{
		Params: params,
		debug:            *isDebug,
		pathToEnvFile:    *pathToEnvFile,
		messageUC:        message_usecase.NewMessageUseCase(messageRepository, centrifugoClient, userManager, params),
		centrifugoClient: centrifugoClient,
		userManager:      userManager,
		logger: l,
	}
}

func (app App) Run() {
	if app.debug {
		fmt.Println("Debug mode running")
		//app.logger.Info("Debug mode running")
	}
	port, _ := strconv.Atoi(os.Getenv("PORT"))
	fmt.Printf("Running server on port %d\n", port)

	systemUser := &env.SystemUser{}


	enableDaemons, enableDaemonsErr:= strconv.ParseBool(os.Getenv("DAEMONS_ENABLE"))
	if enableDaemonsErr == nil && enableDaemons == true {
		go app.messageUC.Daemons()
	}

	messageHandler := message_handler_http.NewHandler(app.messageUC, app.centrifugoClient, app.userManager, app.Params, systemUser, app.logger)
	authHandler := auth.NewAuthHandler(app.userManager, app.debug, systemUser, app.Params)
	router := mux.NewRouter()
	// auth
	router.HandleFunc("/check-session", authHandler.CheckSessionHandler).Methods("GET")
	router.HandleFunc("/get-iin", authHandler.GetInnHandler).Methods("GET")

	// manager auth
	router.HandleFunc("/manager/auth", authHandler.ManagerAuth).Methods("GET")

	// client auth
	router.HandleFunc("/client/auth", authHandler.ClientAuth).Methods("GET")
	// client send message POST
	router.HandleFunc("/client/messages", messageHandler.AddClientMessage).Methods("POST")

	// chats
	router.HandleFunc("/client/chat", messageHandler.GetClientChat).Methods("GET")
	router.HandleFunc("/chat/{id:[0-9]+}/messages", messageHandler.GetChatMessages).Methods("GET")

	// вернет ids чатов которын созданы после from. Параметры: from - unix метка
	router.HandleFunc("/chat/id-list", messageHandler.GetChatIds).Methods("GET")
	// список сообщений по id чата
	router.HandleFunc("/chat/messages-list", messageHandler.GetChatMessages).Methods("GET")

	// messages
	router.HandleFunc("/messages", messageHandler.GetMessages).Methods("GET")
	router.HandleFunc("/messages/start-chat/{id:[0-9]+}", messageHandler.MessagesStartingFromChat).Methods("GET")
	router.HandleFunc("/messages", messageHandler.AddMessage).Methods("POST")
	router.HandleFunc("/messages/read", messageHandler.ReadMessage).Methods("POST")

	// conversations
	router.HandleFunc("/conversations", messageHandler.GetConversations).Methods("GET")
	router.HandleFunc("/conversation", messageHandler.GetConversation).Methods("GET")
	router.HandleFunc("/close-conversation", messageHandler.CloseChat).Methods("GET")

	// get list managers
	router.HandleFunc("/managers", messageHandler.GetManagers).Methods("GET")

	// files
	router.HandleFunc("/upload-file", messageHandler.UploadFile).Methods("POST")
	router.HandleFunc("/get-file", messageHandler.GetFile).Methods("GET")

	router.Use(func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			r.Body = http.MaxBytesReader(w, r.Body, app.Params.MaxFileSize)
			next.ServeHTTP(w, r)
		})
	})
	router.Use(authHandler.AuthFilter)

	headersOk := handlers.AllowedHeaders([]string{"X-Requested-With", "Content-Type", "mid", "sid", "uin", "token", "role"})
	
	originsOk := handlers.AllowedOrigins([]string{"*"})
	methodsOk := handlers.AllowedMethods([]string{"GET", "HEAD", "POST", "PUT", "OPTIONS"})

	serverError := http.ListenAndServe(":"+os.Getenv("PORT"), handlers.CORS(originsOk, headersOk, methodsOk)(router))

	if serverError != nil {
		app.logger.Error(serverError.Error())
		fmt.Println(serverError.Error())
		log.Fatal(serverError)
	}
}