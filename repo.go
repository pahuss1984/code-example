package mysql

import (
	"goCHAT_API/models"
	"time"
)

type Chat struct {
	Id        int `gorm:"primary_key"`
	CreatedAt int
	ClosedAt  int
	UserId    string
	Messages  []Message
	ChatUsers []ChatUser
}

type ChatUser struct {
	ChatId int
	UserId string `gorm:"size:128"`
	JoinedAt int
}

func (r MessageRepository) GetChat(id int) *models.Chat {
	chat := &Chat{}
	r.db.Db.Where("id=?", id).Order("created_at DESC").Find(chat)
	return createChat(chat)
}

func (r MessageRepository) AddChat(chat *models.Chat) int {
	dbchat := createDbChat(chat)
	dbchat.CreatedAt = int(time.Now().Unix())
	r.db.Db.Create(&dbchat)
	return dbchat.Id
}

func (r MessageRepository) DeleteChat(id int) bool {
	r.db.Db.Where("id=?", id).Delete(&Chat{})
	return true
}

func (r MessageRepository) UpdateChat(chat *models.Chat) bool {
	dbchat := createDbChat(chat)
	r.db.Db.Save(dbchat)
	return true
}

func (r MessageRepository) GetLastUserChat(userId string) *models.Chat {
	chat := Chat{}
	r.db.Db.Where("closed_at=0 AND user_id=?", userId).Last(&chat)
	return createChat(&chat)
}

func (r MessageRepository) GetLastUserChatForClientSide(userId string) *models.Chat {
	chat := Chat{}
	r.db.Db.Where("user_id=?", userId).Order("created_at desc").Last(&chat)
	return createChat(&chat)
}

func (r MessageRepository) GetUserChats(userId string) []*models.Chat {
	var chats []*Chat
	r.db.Db.Table("chat").
		Select("DISTINCT chat.*").
		Where("chat.closed_at=0 AND chat_user.user_id = ? OR chat.user_id = ?", userId, userId).
		Joins("LEFT JOIN chat_user ON chat_user.chat_id=chat.id").
		Scan(&chats)
	return createChats(chats)
}

func (r MessageRepository) GetAllOpenedChats() []*models.Chat {
	var chats []*Chat
	r.db.Db.Model(&Chat{}).Where("closed_at=?", 0).Scan(&chats)
	return createChats(chats)
}

func (r MessageRepository) GetSimplexChats(userId string) []*models.Chat {
	var chats []*models.Chat
	var chatId int
	var usersCount int

	rows, _ := r.db.Db.Table("chat_user").
		Select("chat_user.chat_id, COUNT(chat_user.chat_id) AS count").
		Joins("LEFT JOIN chat ON chat_user.chat_id=chat.id AND chat.closed_at=0").
		Group("chat_user.chat_id").
		Rows()
	defer rows.Close()
	for rows.Next() {
		rows.Scan(&chatId, &usersCount)
		if usersCount == 1 {
			chats = append(chats, r.GetChat(chatId))
		}
	}
	return chats
}

func (r MessageRepository) AddChatUser(chatId int, userId string) {
	r.db.Db.Create(&ChatUser{
		UserId: userId,
		ChatId: chatId,
		JoinedAt: int(time.Now().Unix()),
	})
}

func (r MessageRepository) GetChatUsers(chatId int) []*models.ChatUser {
	var cc []*ChatUser
	r.db.Db.Where("chat_id=?", chatId).Order("joined_at ASC").Find(&cc)
	return createChatUsers(cc)
}

func (r MessageRepository) UserInChat(chatId int, userId string) bool {
	var count int
	row := r.db.Db.Table("chat_user").Where("chat_id=? AND user_id=?", chatId, userId).Select("COUNT(*)").Row()
	row.Scan(&count)
	return count > 0
}

func (r MessageRepository) DeleteChatUser(chatUser *models.ChatUser) bool {
	cu := createDbChatUser(chatUser)
	r.db.Db.Where("chat_id=? AND user_id=?", chatUser.ChatId, chatUser.UserId).Delete(&cu)
	return true
}

////////////////

func createChat(c *Chat) *models.Chat {
	return &models.Chat{
		Id:        c.Id,
		CreatedAt: c.CreatedAt,
		UserId:    c.UserId,
		ClosedAt:  c.ClosedAt,
	}
}

func createDbChat(c *models.Chat) *Chat {
	return &Chat{
		Id:        c.Id,
		CreatedAt: c.CreatedAt,
		UserId:    c.UserId,
		ClosedAt:  c.ClosedAt,
	}
}

func createChats(ch []*Chat) []*models.Chat {
	out := make([]*models.Chat, len(ch))
	for i, c := range ch {
		out[i] = createChat(c)
	}
	return out
}

func createChatUser(c *ChatUser) *models.ChatUser {
	return &models.ChatUser{
		ChatId: c.ChatId,
		UserId: c.UserId,
	}
}

func createDbChatUser(c *models.ChatUser) *ChatUser {
	return &ChatUser{
		ChatId: c.ChatId,
		UserId: c.UserId,
	}
}

func createChatUsers(ch []*ChatUser) []*models.ChatUser {
	out := make([]*models.ChatUser, len(ch))
	for i, c := range ch {
		out[i] = createChatUser(c)
	}
	return out
}
