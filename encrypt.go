package encoder

import "fmt"

const (
	RightToLeft = 1
	LeftToRight = 2
)

type Rotor struct {
	Number    int8
	specification     interface{}
	CurrentPosition   int8
	encoderEntrails  *Entrails
	forwardMap        map[byte]byte  // карта прямого преобразования (номер пары по символу)
	reverseMap        map[byte]byte  // карта обратного преобразования (номер пары по символу)
}

func NewRotor(number int8, position int8) (*Rotor, error) {
	err := CreateRotorError{}
	forwardMap  := map[byte]byte{}
	reverseMap  := map[byte]byte{}
	if position > 26 || position < 1 {
		err.SetData("Invalid position")
	}
	specification := getSpecificationByNumber(number)
	for _, p := range specification {
		forwardMap[byte(p[0])] = byte(p[1])
		reverseMap[byte(p[1])] = byte(p[0])
	}
	return &Rotor {
		Number:    number,
		CurrentPosition:   position,
		specification: specification,
		reverseMap: reverseMap,
		forwardMap: forwardMap,
	}, err
}

func (r *Rotor) GetConnectByte(b byte, vector int) byte {
	if vector == 0 || vector > LeftToRight{
		vector = RightToLeft
	}
	b, _ = r.correctByte(b)
	alpCharNumbToChar, _ := InitAlphaBet()
	fmt.Printf("rotor %d  input %s output %s  vector %d\n", r.Number, alpCharNumbToChar[int(b)], alpCharNumbToChar[int(r.getConnect(b, vector))], vector)
	return r.getConnect(b, vector)
}

func (r *Rotor) getConnect(b byte, vector int) byte {
	b, _ = r.correctByte(b)

	if vector == RightToLeft {
		return r.forwardMap[b]
	} else {
		return r.reverseMap[b]
	}
}

// сдвинуть входящий символ на величину начальной позиции ротора
func (r *Rotor) correctByte(b byte) (byte, error) {
	var err error
	if b < 97 || b > 122 {
		err = RotorCharsError{
			data: "invalid input char",
		}
		return b, err
	}
	//b = b - 97 + 1
	b += byte(r.CurrentPosition - 1)
	if b > 122 {
		b = 97 + (b - 122)
	}
	return b, err
}